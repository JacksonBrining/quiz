#include <stdio.h>
#include <string.h>

main()
{
int answer;
int score;
int grade;
int correct = 0;
int wrong = 0;

printf("Please type the number of the answer you want to choose then hit enter\n");

printf("\nQ1) Is global warming real??\n");
printf("[1] Maybe\n[2] I don't care\n[3] Yes\n[4] No\n");
scanf("%d", &answer);

if(answer==3)
{
printf("That's Correct!\n");
correct = ++correct;

}
else
{
printf("Wrong Answer\n");
wrong = ++wrong;

}

printf("\nQ2) Is global warming bad?\n");
printf("[1] Nobody cares\n[2] Yes\n[3] No\n[4] Maybe(?)\n");
scanf("%d", &answer);

if(answer==2)
{
printf("That's Correct!\n");
correct = ++correct;

}
else
{
printf("Wrong Answer\n");
wrong = ++wrong;

}

printf("\nQ3) What will global warming melt?\n");
printf("[1] Water\n[2] Sand\n[3] Ice\n[4] Nothing\n");
scanf("%d", &answer);

if(answer==3)
{
printf("That's Correct!\n");
correct = ++correct;

}
else
{
printf("Wrong Answer\n");
wrong = ++wrong;

}

printf("\nQ4) Should we do something about global warming?\n");
printf("[1] My kids can do something\n[2] Yes\n[3] Why should we?\n[4] Heck no\n");
scanf("%d", &answer);

if(answer==2)
{
printf("That's Correct!\n");
correct = ++correct;

}
else
{
printf("Wrong Answer\n");
wrong = ++wrong;

}

printf("\nQ5)How can we stop global warming?\n");
printf("[1] Smoking....Alot\n[2] Using more plastic\n[3] Making cars with bigger engines\n[4] Use more green materials\n");
scanf("%d", &answer);

if(answer==4)
{
printf("That's Correct!\n\n\n\n");
correct = ++correct;

}
else
{
printf("Wrong Answer\n\n\n\n");
wrong = ++wrong;

}


score = ((correct/5)*100);

if(score >= 90)
{
    grade = 'A';
}
else if(score >= 80)
{
    grade = 'B';
}
else if(score >= 70)
{
    grade = 'C';
}
else if(score >= 60)
{
    grade = 'D';
}
else if(score < 60)
{
    grade = 'F';
}

printf("You got %d correct and %d wrong.\n", correct, wrong);
printf("Your score is a %d\n", score);
printf("Your grade is a %c\n", grade);

}
